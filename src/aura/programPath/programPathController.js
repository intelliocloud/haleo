({
    doInit : function(component,event,helper) {
        helper.retrieveWeeks(component);
    },

    goToPreviousWeek : function(component,event,helper) {
        helper.previousWeek(component);
    },

    markAsUncomplete : function(component,event,helper) {
        helper.uncomplete(component);
    },

    markAsComplete : function(component,event,helper) {
        helper.complete(component);
    },
})