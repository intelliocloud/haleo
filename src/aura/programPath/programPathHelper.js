({
    retrieveWeeks : function(component) {
        this.callServer(component,'getWeeks',{
            serverParams:{
                contactId:component.get("v.contactId")
            }, 
            clientParams:{
                onSuccess: function(response) {
                    var result = response.getReturnValue();
                    component.set("v.weeks",result);
                    //init actions object
                    var actions = {};
                    for(var i = 0;i<result.length;i++) {
                        if(result[i].Current_Week__c) {
                            if(i>0 && !result[i].Completed__c) actions.goToPreviousWeek = true;
                            if(i+1==result.length && result[i].Completed__c)  actions.markAsUncomplete = true;
                            else actions.markAsComplete = true;
                            component.set("v.actions",actions);
                            console.debug(component.get("v.actions"));
                            break;
                        }
                    }
                }
            }

        })
    },

    previousWeek : function(component,event) {
        var self = this;
        self.callServer(component,'previousWeek',{
            serverParams:{
                contactId:component.get("v.contactId")
            },
            clientParams:{
                onSuccess: function(response) {
                   self.retrieveWeeks(component);                    
                }
            }
        })
    }, 

    uncomplete : function(component,event) {
        var self = this;
        self.callServer(component,'uncomplete',{
            serverParams:{
                contactId:component.get("v.contactId")
            },
            clientParams:{
                onSuccess: function(response) {
                   self.retrieveWeeks(component);                    
                }
            }
        })
    }, 

    complete : function(component,event) {
        var self = this;
        self.callServer(component,'complete',{
            serverParams:{
                contactId:component.get("v.contactId")
            },
            clientParams:{
                onSuccess: function(response) {
                   self.retrieveWeeks(component);                    
                }
            }
        })
    }, 
})