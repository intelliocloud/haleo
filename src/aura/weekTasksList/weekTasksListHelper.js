({
    getTasks : function(component) {
        this.callServer(component,'getTasks',{
            serverParams : {
                patientWeekId:component.get("v.recordId")
            },
            clientParams : {
                onSuccess : function(response) {
                    component.set("v.tasks",response.getReturnValue());
                }
            }

        })
    }
})