({
    handleTaskClick : function(component,event,helper) {
        var task=component.get("v.task");
        if(task.Type__c == "other") helper.navigateToURL(component,task.ResourceLink__c);
        else helper.openModal(component,task);
    }
})