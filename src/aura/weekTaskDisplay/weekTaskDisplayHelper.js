({
	navigateToURL : function(component,link) {
        $A.get("e.force:navigateToURL").setParams({url:link}).fire();
	},
    
    openModal : function(component,task) {
		var modal = component.find("modal");
        if(modal) modal.set("v.hide",false);
        else $A.createComponent("c:weekTaskRessourceDisplay",
                           {
                               name:task.ResourceName__c,
                               type:task.ResourceType__c.toLowerCase(),
                               link:task.ResourceId__c,
                               "aura:id":"modal"
                           },
                           function(modal, status, errorMessage) {
                               
                               if (status === "SUCCESS") {      
                                   var body = component.get("v.body");
                                   body.push(modal);
                                   component.set("v.body",body);
                               } else if (status === "INCOMPLETE") {
                                   console.log("No response from server or client is offline.")
                                   // Show offline error
                               } else if (status === "ERROR") {
                                   console.log("Error: " + errorMessage);
                                   // Show error message
                               }
                           }
                          );
	}
})