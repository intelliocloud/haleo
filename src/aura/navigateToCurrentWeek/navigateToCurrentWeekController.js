({
    doInit : function(component,event,helper) {
        if(Boolean($A.get("$Label.c.icCommunityHomeRedirection"))) helper.navigateToCurrentWeek(component);
    }
})