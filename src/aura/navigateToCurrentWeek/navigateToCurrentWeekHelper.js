({
    navigateToCurrentWeek : function(component,event) {
        this.callServer(component,'getCurrentWeek',{
            serverParams:{
            },
            clientParams:{
                onSuccess: function(response) {                   
                    var result = response.getReturnValue();                    
                    console.debug("result current week",result);
                    if(result) $A.get("e.force:navigateToSObject").setParams({"recordId": result}).fire();
                    else component.set("v.loading",false);                   
            }, 
                onError : function(response) {
                    component.set("v.loading",false);
                }
            }
        })
    }
})