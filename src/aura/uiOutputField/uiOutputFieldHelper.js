({
    getValue: function (component) {
        this.callServer(component, 'getValue', {
            serverParams: {
                recordId: component.get("v.recordId"),
                sObjectName: component.get("v.sObjectName"),
                fieldAPI: component.get("v.fieldAPI")
            },
            clientParams: {
                onSuccess: function (response) {
                    component.set("v.value", response.getReturnValue());
                }
            }
        });
    }

})