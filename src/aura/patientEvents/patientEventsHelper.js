({
    getEvents : function(component,event) {
        this.callServer(component,'getPatientEvents',{
            serverParams:{
                
            },
            clientParams:{
                onSuccess: function(response) {
                    var result = response.getReturnValue();
                    console.debug("result patient events",result);
                    component.set("v.events",result);
                    
                }
            }
        });
    }
})