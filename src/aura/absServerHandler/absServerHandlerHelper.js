({
    callServer: function (component, actionName, params) {
        var clientParams = params.clientParams;
        var serverParams = params.serverParams;
        var action = component.get("c." + actionName);

        action.setParams(serverParams);
        action.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                if (clientParams.onSuccess) clientParams.onSuccess(response); //trigger function onSuccess()
            } else if (response.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) { //by default, log the error with the message
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }

                if (clientParams.onError) clientParams.onError(response); //trigger function onError()
            }
        });

        action.setCallback(this, function (response) {
            console.log(actionName + " ABORTED");
            if (clientParams.onAbort) clientParams.onAbort(response);
        }, "ABORTED");

        if (clientParams.abortable) action.setAbortable();
        if (clientParams.storable) action.setStorable();

        $A.enqueueAction(action);
    }
})