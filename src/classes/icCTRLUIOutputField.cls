public class icCTRLUIOutputField {
    @AuraEnabled
    public static string getValue(String recordId, String sObjectName, String fieldAPI) {
        SObject obj;
        String query = 'SELECT ';

        query+=fieldAPI;
        query+=' FROM '+sObjectName;
        query+=' WHERE Id = \''+recordId+'\'';
        try {
             obj = Database.query(query);
         } catch(Exception e) {
             system.debug(e.getMessage());
         }
         if(obj!= null) {
             if(fieldAPI.contains('__r.')) {
                 String[] parentField = fieldApi.split('\\.');
                 return (String)(obj.getSObject(parentField[0]).get(parentField[1]));
             } 
             else return (String)(obj.get(fieldAPI));
         }
         else return '';
    }
}