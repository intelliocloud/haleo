public class icDTOEvent {
	public String subject { get; set; }
	public String therapistName { get; set; }
	public Datetime startDatetime { get; set; }
	public Datetime endDatetime { get; set; }
	public String webLink { get; set; }	
}