public class icCTRLWeekTasksList {
    @AuraEnabled
    public static WeekTask__c[] getTasks(String patientWeekId) {
        String weekId = ''; 
        try {
            weekId = [SELECT Week__c FROM PatientWeek__c WHERE Id = :patientWeekId].Week__c;
        } catch(Exception e) {
            system.debug(e.getMessage());
        }
        return [SELECT ID, Name,NameFr__c, DescriptionEn__c, DescriptionFr__c,ResourceName__c, ResourceId__c, ResourceType__c From WeekTask__c WHERE Week__c = :weekId Order By Sequence__c]; 
    }
}