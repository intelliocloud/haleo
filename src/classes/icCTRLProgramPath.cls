public class icCTRLProgramPath {
    @AuraEnabled
    public static PatientWeek__c[] getWeeks(String contactId) {
        PatientWeek__c[] patientWeeks = new List<PatientWeek__c>();
        if(contactId == '' || contactId == null) {
             contactId = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()].ContactId;            
        }
        patientWeeks = [SELECT Id, Completed__c, Current_Week__c, Week__r.Name, Week__r.NameFr__c, Week__r.Sequence__c FROM PatientWeek__c WHERE Patient__c = :contactId Order By Week__r.Sequence__c];
        system.debug('####contactId :' + contactId);
        system.debug('####patientWeeks :' + patientWeeks);
        return patientWeeks;
    }

    @AuraEnabled
    public static void previousWeek(String contactId) {
        PatientWeek__c[] patientWeeks = new List<PatientWeek__c>();
        patientWeeks = [SELECT Id, Completed__c, Current_Week__c, Week__r.Name, Week__r.NameFr__c, Week__r.Sequence__c FROM PatientWeek__c WHERE Patient__c = :contactId Order By Week__r.Sequence__c];

        for(Integer i = 0;i<patientWeeks.size();i++) {
            if(patientWeeks[i].Current_Week__c) {              
                patientWeeks[i].Current_Week__c = false;
                patientWeeks[i-1].Current_Week__c = true;
                patientWeeks[i-1].Completed__c = false;
                break;
            }
            
        }
        try {
            update(patientWeeks);
        } catch (DMLException e) {
            system.debug('##DML Exception : '+e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void uncomplete(String contactId) {
        PatientWeek__c[] patientWeeks = new List<PatientWeek__c>();
        patientWeeks = [SELECT Id, Completed__c, Current_Week__c, Week__r.Name, Week__r.NameFr__c, Week__r.Sequence__c FROM PatientWeek__c WHERE Patient__c = :contactId Order By Week__r.Sequence__c];

        for(Integer i = 0;i<patientWeeks.size();i++) {
            if(patientWeeks[i].Current_Week__c) {              
                patientWeeks[i].Completed__c = false;
                break;
            }
            
        }
        try {
            update(patientWeeks);
        } catch (DMLException e) {
            system.debug('##DML Exception : '+e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void complete(String contactId) {
        PatientWeek__c[] patientWeeks = new List<PatientWeek__c>();
        patientWeeks = [SELECT Id, Completed__c, Current_Week__c, Week__r.Name, Week__r.NameFr__c, Week__r.Sequence__c FROM PatientWeek__c WHERE Patient__c = :contactId Order By Week__r.Sequence__c];

        for(Integer i = 0;i<patientWeeks.size();i++) {
            if(patientWeeks[i].Current_Week__c) {              
                patientWeeks[i].Completed__c = true;
                //change current week only if there is a next one
                if(i<patientWeeks.size()-1) {
                    patientWeeks[i].Current_Week__c = false;
                    patientWeeks[i+1].Current_Week__c = true;
                }
                break;
            }
            
        }
        try {
            update(patientWeeks);
        } catch (DMLException e) {
            system.debug('##DML Exception : '+e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }
}