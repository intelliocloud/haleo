public class icCTRLPatientEvents {
	@AuraEnabled
	public static Event[] getPatientEvents() {
		icBusinessLogicSOAP.IClass blSOAP = (icBusinessLogicSOAP.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicSOAP');
		
		String newSessionId = blSOAP.getSessionId();
		
		return blSOAP.getUpcomingEventsForCommunityUserContact(newSessionId, UserInfo.getUserId());
	}
}