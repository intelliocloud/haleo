public class icCTRLNavigateToCurrentWeek {
    @AuraEnabled
    public static Id getCurrentWeek(String contactId) {
        if(contactId == '' || contactId == null) {
             contactId = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()].ContactId;            
        }
        PatientWeek__c currentWeek = [SELECT Id FROM PatientWeek__c WHERE Patient__c = :contactId AND Current_Week__c = true] ;
        system.debug('###currentWeek :'+currentWeek);
        return currentWeek == null ? '' : currentWeek.Id;
    }
}